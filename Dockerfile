FROM ubuntu
MAINTAINER "GitLab Production Team <ops-contact@gitlab.com>"
RUN apt-get update; apt-get install -y wget; apt-get clean
RUN apt-get update; apt-get install -y apache2-utils; apt-get clean
RUN wget -q https://github.com/GoogleCloudPlatform/pubsubbeat/releases/download/1.3.0-rc/linux.tar.gz && tar -xvf linux.tar.gz && mv /linux/ /opt/pubsubbeat/
WORKDIR /opt/pubsubbeat
CMD ./pubsubbeat
